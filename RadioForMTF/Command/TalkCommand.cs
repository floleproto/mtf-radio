﻿using RadioForMTF;
using RadioForMTF.API;
using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;
using System.Linq;

namespace Command
{
    internal class TalkCommand : IEventHandlerCallCommand
    {
        private Main main;

        public TalkCommand(Main main)
        {
            this.main = main;
        }

        public void Execute(string txt, Player p)
        {

                

                switch (p.TeamRole.Role)
                {
                    case Smod2.API.Role.NTF_COMMANDER:
                        API.SendMsgToMTF($"{LangManager.Manager.GetTranslation("rmtf.role.commander").Replace("\\n", "\n").Replace("[name]", p.Name)}\n{txt}");
                        break;

                    case Smod2.API.Role.NTF_LIEUTENANT:
                        API.SendMsgToMTF($"{LangManager.Manager.GetTranslation("rmtf.role.lieutenant").Replace("\\n", "\n").Replace("[name]", p.Name)}\n{txt}");
                        break;

                    case Smod2.API.Role.NTF_SCIENTIST:
                        API.SendMsgToMTF($"{LangManager.Manager.GetTranslation("rmtf.role.lieutenant").Replace("\\n", "\n").Replace("[name]", p.Name)}\n{txt}");
                        break;

                    case Smod2.API.Role.NTF_CADET:
                        API.SendMsgToMTF($"{LangManager.Manager.GetTranslation("rmtf.role.cadet").Replace("\\n", "\n").Replace("[name]", p.Name)}{txt}\n");
                        break;

                    case Smod2.API.Role.FACILITY_GUARD:
                        API.SendMsgToMTF($"{LangManager.Manager.GetTranslation("rmtf.role.guard").Replace("\\n", "\n").Replace("[name]", p.Name)}{txt}\n");
                        break;
                }
        }

        public void OnCallCommand(PlayerCallCommandEvent ev)
        {
            string[] cmd = ev.Command.Split(' ');

            if(cmd[0] == "radio")
            {
                
                if (!ConfigManager.Manager.Config.GetBoolValue("mtfr_allow_without_radio", true) && !ev.Player.HasItem(ItemType.RADIO))
                {
                    ev.ReturnMessage = LangManager.Manager.GetTranslation("rmtf.commands.notradio").Replace("\\n", "\n");
                    return;
                }

                if (ev.Player.TeamRole.Team == Smod2.API.Team.NINETAILFOX)
                {
                    if (cmd.Length > 1)
                    {
                        int i = 1;
                        string txt = "";

                        do
                        {
                            txt = txt + cmd[i] + " ";
                            i++;

                        } while (i != cmd.Length);

                        Execute(txt, ev.Player);
                        ev.ReturnMessage = LangManager.Manager.GetTranslation("rmtf.commands.success").Replace("\\n", "\n");

                    }
                    else
                    {
                        ev.ReturnMessage = LangManager.Manager.GetTranslation("rmtf.commands.missing_argument").Replace("\\n", "\n");
                    }
                }
                else
                {
                    ev.ReturnMessage = LangManager.Manager.GetTranslation("rmtf.commands.notmtf").Replace("\\n", "\n");
                }
                
            }
        }
    }
}