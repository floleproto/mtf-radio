﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RadioForMTF.API;
using Smod2;
using Smod2.Attributes;

namespace RadioForMTF
{
    [PluginDetails(author = "Flo - Fan", configPrefix = "mtfr", description = "A configurable radio for the MTF", id = "flo.rmtf", langFile = "mtfradio", name = "MTF Radio", SmodMajor = 3, SmodMinor = 4, SmodRevision = 0, version = "0.0.1")]

    public class Main : Plugin
    {

        public override void OnDisable()
        {
            this.Info("MTF Radio is now disabled.");
        }

        public override void OnEnable()
        {
            this.Info("MTF Radio is now enabled.");
        }

        public override void Register()
        {
            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_enabled", true, true, "Enable the plugin or not."));

            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_allow_without_radio", true, true, "Allow MTF to talk and get information in this radio without the radio item in them inventory."));

            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_cispawn", true, true, "Notify MTFs when Chaos Insurgency spawn."));
            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_mtfspawn", true, true, "Notify MTFs when a MTF squad spawn."));

            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_mtfdie", true, true, "Notify MTFs when a another MTF die."));
            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_scpdie", true, true, "Notify MTFs when a SCP die."));

            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_generator", true, true, "Notify MTFs when a generator is activated."));

            this.AddConfig(new Smod2.Config.ConfigSetting("mtfr_talk", true, true, "Allow MTFs to talk in the plugin's radio."));


            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.prefix", "<color=#15b500><b>[MTF RADIO]</b></color>\\n<color=#ff9000>[txt]</color>", "mtfradio"));

            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.generator.finish", "A generator is now activated.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.dead.mtf", "Soldier [victim] in on the ground.\\n[nb_alive] NTF remaining.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.dead.scp", "[scp] is now contained.\\n[nb_scpalive] SCP remaining.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.spawn", "<color=#15b500><b>[MTF RADIO]</b>\\n<color=red>Central</color></color>\\n<size=30>Alert, a containment breach has been detected.\\nYour radio has been connected to the MTF Radio.\\nWe don't know wich SCPs are outside there cell.</size>", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.respawn.ci", "Alert, intrusion detected.\\nChaos Insurgency spoted.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.msg.respawn.mtf", "A NTF unit has entered into the Facility.\\nThere are [nb_mtf_count] soldier in this unit.", "mtfradio"));

            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.commands.success", "The message has been sent", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.commands.missing_argument", "Missing argument : .radio <Text>", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.commands.notmtf", "You're not a MTF.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.commands.notradio", "You havn't got a radio.", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.commands.preset", "List of the presets : [preset]", "mtfradio"));

            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.role.commander", "<color=#003BFF>Commander [name]</color>\n", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.role.lieutenant", "<color=#0096FF>Lieutenant [name]</color>\n", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.role.cadet", "<color=#69E2FF>Cadet [name]</color>\n", "mtfradio"));
            this.AddTranslation(new Smod2.Lang.LangSetting("rmtf.role.guard", "<color=#828282>Guard [name]</color>\n", "mtfradio"));

            if (ConfigManager.Manager.Config.GetBoolValue("mtfr_enabled", true))
            {
                this.AddEventHandlers(new Handler.GlobalHandler(this));
                this.AddEventHandlers(new Command.TalkCommand(this));
            }
        }
    }
}
