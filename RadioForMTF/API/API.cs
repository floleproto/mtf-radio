﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.API;

namespace RadioForMTF.API
{
    class API
    {
        public static void SendMsgToMTF(string txt)
        {

            bool allowRadio = ConfigManager.Manager.Config.GetBoolValue("mtfr_allow_without_radio", true);
            string prefix = LangManager.Manager.GetTranslation("rmtf.prefix").Replace("[txt]", txt).Replace("\\n", "\n");

            foreach (Player p in PluginManager.Manager.Server.GetPlayers())
            {
                if(p.TeamRole.Team == Team.NINETAILFOX)
                {
                    if (allowRadio)
                    {
                        p.PersonalClearBroadcasts();
                        p.PersonalBroadcast(5, prefix, true);
                        continue;
                    }
                    else
                    {
                        foreach(Item i in p.GetInventory())
                        {
                            if(i.ItemType == ItemType.RADIO)
                            {
                                p.PersonalClearBroadcasts();
                                p.PersonalBroadcast(5, prefix, true);
                                break;
                            }
                        }
                        continue;
                    }
                }
                continue;
            }
        }
    }
}